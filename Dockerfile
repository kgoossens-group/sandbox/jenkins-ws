FROM maven:latest

COPY initial/target/gs-maven-0.1.0.jar /usr/local/gs-maven-0.1.0.jar

ENTRYPOINT ["java","-jar","/usr/local/gs-maven-0.1.0.jar"]
